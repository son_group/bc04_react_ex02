import React, { Component } from "react";
import WearGlass from "./WearGlass";

export default class Glass_Model extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div
            className="border border-1 "
            style={{
              position: "relative",
            }}
          >
            <div
              style={{
                background: 'url("./glassesImage/model.jpg")',
                backgroundPosition: "center top",
                backgroundSize: "auto",
                height: "500px",
                backgroundRepeat: "no-repeat",
              }}
            >
              <WearGlass glassItem={this.props.glassItem} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
