import React, { Component } from "react";
import Glass_Model from "./Glass_Model";
import { dataGlass } from "./dataGlasses";
import Glass_List from "./Glass_List";
import WearGlass from "./WearGlass";

export default class Glass_Layout extends Component {
  state = {
    glassArr: dataGlass,
    glassDetail: {},
  };

  handleViewGlass = (glassID) => {
    let index = this.state.glassArr.findIndex((item) => {
      return item.id == glassID;
    });
    index !== -1 &&
      this.setState({
        glassDetail: this.state.glassArr[index],
      });
  };

  render() {
    return (
      <div>
        {/* <p>Test</p> */}
        <Glass_Model glassItem={this.state.glassDetail} />

        <Glass_List
          glassList={this.state.glassArr}
          handleViewGlass={this.handleViewGlass}
        />
      </div>
    );
  }
}
