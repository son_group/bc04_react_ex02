import React, { Component } from "react";
import Glass_Item from "./Glass_Item";

export default class Glass_List extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="row justify-content-center py-5 ">
            {this.props.glassList.map((item, index) => {
              return (
                <div>
                  <Glass_Item
                    key={index.toString() + item.id}
                    glassItem={item}
                    handleViewGlass={this.props.handleViewGlass}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
