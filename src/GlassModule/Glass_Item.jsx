import React, { Component, Fragment } from "react";

export default class Glass_Item extends Component {
  render() {
    let item = this.props.glassItem;
    return (
      <Fragment>
        <div
          className="card img-fluid border-0 mx-2"
          style={{ width: "100px" }}
        >
          <img
            onClick={() => {
              this.props.handleViewGlass(item.id);
            }}
            className="card-img-top"
            src={item.url}
            alt="Card image cap"
          />
        </div>
      </Fragment>
    );
  }
}
