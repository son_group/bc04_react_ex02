import React, { Component } from "react";

export default class WearGlass extends Component {
  render() {
    let { name, url, price, desc } = this.props.glassItem;
    return (
      <div>
        <img
          style={{
            position: "absolute",
            display: "block",
            top: "28%",
            left: "39.5%",
            width: 238,
            height: 85,
            opacity: "0.9",
          }}
          src={url}
        />
        <div
          className="text-center"
          style={{
            position: "absolute",
            bottom: 0,
            left: "20%",
            backgroundColor: "white",
            opacity: "0.7",
          }}
        >
          <h3 className="mb-0">{name}</h3>
          <p className="mb-0 font-weight-bold"> {price}</p>
          <p>{desc}</p>
        </div>
      </div>
    );
  }
}
